﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
    // consts //
    private new string name = "/gamesave.save";
    // end - consts //

    public void SaveGame()
    {
        SaveGame save = GameManager.getUserDataToSave();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + name);
        bf.Serialize(file, save);
        file.Close();
    }

    public void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + name))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + name, FileMode.Open);
            SaveGame save = (SaveGame)bf.Deserialize(file);
            file.Close();
            Debug.Log("Game Loaded");

            return GameManager.loadGame(save);
        }
        else
        {
            Debug.Log("No game saved!");
        }
    }
}
