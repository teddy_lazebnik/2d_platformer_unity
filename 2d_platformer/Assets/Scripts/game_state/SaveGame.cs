﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manage the saving of the game's information
/// </summary>
[System.Serializable]
public class SaveGame
{
    // the data we wish to remember about the game //
    public UserSettings userSettings;
    public UserShop userShop;
    public int latest_level;
    // end - the data we wish to remember about the game //
}
