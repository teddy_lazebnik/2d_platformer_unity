﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // members //
    public UserSettings userSettings;
    public UserShop userShop;
    public List<PlayerManager> team1;
    public List<PlayerManager> team2;
    public int last_level;
    public int secondsFromStart;
    public int turn;
    public int score;

    // end - members //

    // Start is called before the first frame update
    void Start()
    { 
        this.score = 0;
        this.turn = 1;
        this.userShop = userShop.getDefault();
        this.userSettings = UserSettings.getDefault();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// load all user data for saving
    /// </summary>
    /// <returns>Object to save </returns>
    internal static SaveGame getUserDataToSave()
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// load this instance from the data
    /// </summary>
    /// <param name="save"></param>
    internal static void loadGame(SaveGame save)
    {
        throw new NotImplementedException();
    }
}
