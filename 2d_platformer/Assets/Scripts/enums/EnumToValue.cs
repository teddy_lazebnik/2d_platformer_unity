﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumToValue
{
    public static string getValue(SceneNames name)
    {
        switch (name)
        {
            case SceneNames.Levels:
                return "levels";
            case SceneNames.Menu:
                return "menu";
            case SceneNames.Settings:
                return "settings";
            case SceneNames.Tutorial:
                return "tutorial";
            case SceneNames.Shop:
                return "shop";
            case SceneNames.Game_level:
                return "game_level_";
            default:
                return "";
        }
    }
    public static int getValue(GameLevel name)
    {
        switch (name)
        {
            case GameLevel.Easy:
                return 1;
            case GameLevel.Medium:
                return 2;
            case GameLevel.Hard:
                return 3;
            default:
                return 2;
        }
    }
}
