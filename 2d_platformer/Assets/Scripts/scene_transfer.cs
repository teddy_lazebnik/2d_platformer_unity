﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scene_transfer : MonoBehaviour
{
    public void LoadMenuScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Menu));
    }

    public void LoadLevelsScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Levels));
    }

    public void LoadShopScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Shop));
    }

    public void LoadTutorialScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Tutorial));
    }

    public void LoadSettingsScene()
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Settings));
    }

    public void LoadGameLevelScene(int levelIndex)
    {
        SceneManager.LoadScene(EnumToValue.getValue(SceneNames.Settings) + levelIndex.ToString());
    }
}
